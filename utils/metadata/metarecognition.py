import cv2
from PIL import Image
import pytesseract
import json
import os


path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'assets', 'cameradb.json')
with open(path, 'r', encoding='windows-1251') as f:
    cameradb = json.load(f)

def scale_crop_symbol_data_line(img, coef=5):
    return img.crop((42, 0, img.size[0], img.size[1])).resize((img.size[0]*coef, img.size[1]*coef), Image.ANTIALIAS)

def scale_data_line(img, coef=5):
    return img.resize((img.size[0]*coef, img.size[1]*coef), Image.ANTIALIAS)

def process_text(input_str):
    if input_str:
        input_str = ''.join(filter(lambda key: key not in ['\n','\t','\v','\x0c','"',"'",'’‎',',','.'], input_str)).lower()
    return input_str

def fix_datetime(input_str):
    if input_str:
        input_str = ''.join(filter(lambda key: key not in ['(',')','{','}','[',']'], input_str))
    return input_str

def fix_id(input_str):
    if input_str:
        input_str = input_str.replace('.', '_').replace(' ', '_').replace('__', '_')
        if input_str[-2] != "_":
            input_str = input_str[:-1] + '_' + input_str[-1:]
    return input_str

def adress_from_db(cctv_id):
    return cameradb.get(cctv_id, None)

def get_data(img):
    imgwidth = img.shape[1]
    cctv_data = img[0:64, 0:imgwidth]
    hsv = cv2.cvtColor(cctv_data, cv2.COLOR_BGR2HSV)
    mask = cv2.inRange(hsv, (0, 0, 0), (180, 255, 10))
    check_cctv_data = cv2.countNonZero(mask)
    cctv_data = cv2.cvtColor(cctv_data, cv2.COLOR_BGR2RGB)
    cctv_data = Image.fromarray(cctv_data)
    if check_cctv_data > imgwidth*64*0.9:
        cctv_data_lines = [cctv_data.crop((0, 0+16*i, imgwidth, 16*(i+1))) for i in range(4)]
        cctv_id = fix_id(process_text(pytesseract.image_to_string(scale_crop_symbol_data_line(cctv_data_lines[0]), lang='eng')))
        cctv_name, cctv_adress, cctv_datetime = "", "", ""
        if cctv_id:
            # cctv_name = process_text(pytesseract.image_to_string(scale_data_line(cctv_data_lines[1]), lang='rus')) # deprecated
            cctv_adress = adress_from_db(cctv_id)
            if not cctv_adress:
                cctv_adress = process_text(pytesseract.image_to_string(scale_data_line(cctv_data_lines[2]), lang='rus'))
            cctv_datetime = fix_datetime(process_text(pytesseract.image_to_string(scale_data_line(cctv_data_lines[3]), lang='rus')))
        return {"id": cctv_id, "name": cctv_name, "adress": cctv_adress, "datetime": cctv_datetime}
    else:
        return {"id": "", "name": "", "adress": "", "datetime": ""}
