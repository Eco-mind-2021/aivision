import argparse

import torch
import torch.backends.cudnn as cudnn
import numpy as np
import PIL.Image as pil_image
import cv2
import os

from frscnnmodels import FSRCNN
from frscnnutils import convert_ycbcr_to_rgb, preprocess, calc_psnr


def fsrcnn_scale(src_img, scale=3, weights_file='fsrcnn_x3.pth'):
    weights_file = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'data', weights_file)

    cudnn.benchmark = True
    device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

    model = FSRCNN(scale_factor=scale).to(device)
    state_dict = model.state_dict()
    for n, p in torch.load(weights_file, map_location=lambda storage, loc: storage).items():
          if n in state_dict.keys():
              state_dict[n].copy_(p)
          else:
              raise KeyError(n)
    model.eval()

    image = pil_image.fromarray(src_img).convert('RGB')
    image_width = round(image.width * scale)
    image_height = round(image.height * scale)
    hr = image.resize((image_width, image_height), resample=pil_image.BICUBIC)
    lr = hr.resize((hr.width // scale, hr.height // scale), resample=pil_image.BICUBIC)
    bicubic = lr.resize((lr.width * scale, lr.height * scale), resample=pil_image.BICUBIC)

    lr, _ = preprocess(lr, device)
    hr, _ = preprocess(hr, device)
    _, ycbcr = preprocess(bicubic, device)

    with torch.no_grad():
          preds = model(lr).clamp(0.0, 1.0)
    psnr = calc_psnr(hr, preds)
    preds = preds.mul(255.0).cpu().numpy().squeeze(0).squeeze(0)

    output = np.array([preds, ycbcr[..., 1], ycbcr[..., 2]]).transpose([1, 2, 0])
    output = np.clip(convert_ycbcr_to_rgb(output), 0.0, 255.0).astype(np.uint8)
    return output

def up_scale(img, interpolation=cv2.INTER_NEAREST):
    img = fsrcnn_scale(img)
    height, width = img.shape[:2]
    scale_power = 224/width
    new_size = (round(width*scale_power), round(height*scale_power))
    img = cv2.resize(img, new_size, interpolation=interpolation)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    return img
