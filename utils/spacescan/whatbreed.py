import torch
import torch.nn as nn
from torch.autograd import Variable
from torchvision import transforms, models
from PIL import Image
import os


model_transfer = models.resnet34()
num_ftrs = model_transfer.fc.in_features
model_transfer.fc = nn.Linear(num_ftrs, 120)
path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'weights', 'breeds.pth')
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
checkpoint = torch.load(path, map_location=device)
for key in list(checkpoint.keys()):
    if 'network.' in key:
        checkpoint[key.replace('network.', '')] = checkpoint[key]
        del checkpoint[key]
model_transfer.load_state_dict(checkpoint)
model_transfer.to(device)
model_transfer.eval()

transformation = transforms.Compose([
    transforms.Resize((150,150)),
    transforms.ToTensor(),
    transforms.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225))
])

def predict(img):
    if img.shape[0]*img.shape[1]>1500:
        img = Image.fromarray(img)
        img_tnsr = transformation(img).float()
        img_tnsr = img_tnsr.unsqueeze_(0)
        img = Variable(img_tnsr.to(device))
        output = model_transfer(img).cpu().data.numpy()[0]
        # index1 = output.argmax() # top 1
        index3 = (-output).argsort()[:3] # top 3
        return index3.tolist()
    else:
        return []
