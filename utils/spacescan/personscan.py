import numpy as np


def get_distance(x1, y1, x2, y2):
    return ((x2-x1)**2+(y2-y1)**2)**0.5

def get_near(dog, persons):
    _dist = 3
    x1, y1 = dog["coordinates"]["x"], dog["coordinates"]["x"]
    halfwh = (dog["coordinates"]["width"]+dog["coordinates"]["height"])/2*_dist
    owner_distance = 100
    owner_class = 0

    for person in persons:
        x2, y2 = person["coordinates"]["x"], person["coordinates"]["x"]
        distance = get_distance(x1, y1, x2, y2)/halfwh
        if distance < owner_distance:
            owner_distance = distance
    if owner_distance < 1:
        owner_class = 1
    else:
        owner_distance = 0
    return float(owner_distance), owner_class
