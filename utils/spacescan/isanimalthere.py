import os
import torch
from torch import nn
from torchvision import transforms, models
from torch.autograd import Variable
from PIL import Image


model = models.densenet201()
model.classifier = nn.Sequential(nn.Linear(1920, 512),
                                 nn.ReLU(),
                                 nn.Dropout(0.2),
                                 nn.Linear(512, 256),
                                 nn.ReLU(),
                                 nn.Dropout(0.1),
                                 nn.Linear(256, 2),
                                 nn.LogSoftmax(dim=1))

path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'weights', 'isitdog.pth')
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
state_dict = torch.load(path, map_location=device)['state_dict']
model.load_state_dict(state_dict, strict=False)
model = model.eval()
model.to(device)
image_transforms =transforms.Compose([transforms.Resize(224),
                                      transforms.ToTensor(),
                                      transforms.Normalize([0.485, 0.456, 0.406],
                                                           [0.229, 0.224, 0.225])])


def check_dog(image):
    image = Image.fromarray(image)
    image = image_transforms(image).float()
    image = image.unsqueeze(0).to(device)
    output = model(image)
    _, predicted = torch.max(output.data, 1)
    predicted = int(predicted.item())
    return predicted # 1 - dog, 0 - other
