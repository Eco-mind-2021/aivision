import sys
sys.path.append("../")

"""
File for using the model on a single directory
"""

from jointsmodel import Model

import torch, os
import argparse

from dataset_dogs.demo_dataset import DemoDataset
from torch.utils.data import DataLoader
# from global_utils.helpers.visualize import Visualizer

import cv2
import numpy as np
from tqdm import tqdm

nn = torch.nn

def tail_length(obj_joints):
    obj_joints_tail = obj_joints[12:14,:]
    obj_tail_length = sum((obj_joints_tail[0,:2]-obj_joints_tail[1,:2])**2)**0.5
    if obj_tail_length < 0.1914:
        obj_tail_class = 1 # short
    else:
        obj_tail_class = 2 # long
    return obj_tail_length, obj_tail_class

def get(checkpoint="3501_00034_betas_v4.pth", src_img=None, src_dir="", shape_family_id=-1, batch_size=1, gpu_ids="0", device=None):
    """Run evaluation on the datasets and metrics we report in the paper. """
    checkpoint = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'data', 'pretrained', checkpoint)

    if not device:
        device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    if not os.path.exists(checkpoint):
        print (f"Unable to find: {checkpoint}")

    # os.makedirs(result_dir, exist_ok=True)

    model = load_model_from_disk(
        checkpoint, shape_family_id,
        False, device)

    # Load DataLoader
    if not src_dir:
        dataset = DemoDataset(img=src_img)
    else:
        dataset = DemoDataset(img_dir=src_dir)
    data_loader = DataLoader(dataset, batch_size=batch_size, shuffle=False)  # , num_workers=num_workers)

    # Store smal parameters
    # smal_pose = np.zeros((len(dataset), 105))
    # smal_betas = np.zeros((len(dataset), 26))
    # smal_camera = np.zeros((len(dataset), 3))
    # smal_joints3d = np.zeros((len(dataset), 20, 3))
    # smal_imgname = []
    # smal_has_bbox = []

    # Iterate over the entire dataset
    res_joints_preds = []
    tqdm_iterator = tqdm(data_loader, desc='Eval', total=len(data_loader), disable=True)
    for step, batch in enumerate(tqdm_iterator):
        with torch.no_grad():
            # preds = model(batch, demo=True)
            preds = model(batch)

            # make sure we dont overwrite something
            assert not any(k in preds for k in batch.keys())
            preds.update(batch)  # merge everything into one big dict

        curr_batch_size = preds['img'].shape[0]

        # smal_pose[step * batch_size:step * batch_size + curr_batch_size] = preds['pose'].data.cpu().numpy()
        # smal_betas[step * batch_size:step * batch_size + curr_batch_size] = preds['betas'].data.cpu().numpy()
        # smal_camera[step * batch_size:step * batch_size + curr_batch_size] = preds['camera'].data.cpu().numpy()
        # smal_joints3d[step * batch_size:step * batch_size + curr_batch_size] = preds['joints_3d'].data.cpu().numpy()

        # output_figs = np.transpose(
        #     Visualizer.generate_demo_output(preds).data.cpu().numpy(),
        #     (0, 1, 3, 4, 2))
        for img_id in range(len(preds['imgname'])):
            res_joints_preds.append(preds['joints_3d'].data.cpu().numpy()[0])
            # res_joints_preds.append(preds['camera'])

            # imgname = preds['imgname'][img_id].replace("\\", "/")  # always keep in / format
            # output_fig_list = output_figs[img_id]

            # path_parts = imgname.split('/')
            # smal_imgname.append("{0}/{1}".format(path_parts[-2], path_parts[-1]))
            # path_suffix = "{0}_{1}".format(path_parts[-2], path_parts[-1])
            # img_file = os.path.join(result_dir, path_suffix)
            # output_fig = np.hstack(output_fig_list)
            # smal_has_bbox.append(preds['has_bbox'][img_id])
            # cv2.imwrite(img_file, output_fig[:, :, ::-1] * 255.0)

            # npz_file = "{0}.npz".format(os.path.splitext(img_file)[0])
            # np.savez_compressed(npz_file,
            #                     imgname=preds['imgname'][img_id],
            #                     pose=preds['pose'][img_id].data.cpu().numpy(),
            #                     betas=preds['betas'][img_id].data.cpu().numpy(),
            #                     camera=preds['camera'][img_id].data.cpu().numpy(),
            #                     trans=preds['trans'][img_id].data.cpu().numpy(),
            #                     has_bbox=preds['has_bbox'][img_id])

    # Save reconstructions to a file for further processing
    # param_file = os.path.join(result_dir, 'params.npz')
    # np.savez(param_file,
    #          pose=smal_pose,
    #          betas=smal_betas,
    #          camera=smal_camera,
    #          joints3d=smal_joints3d,
    #          imgname=smal_imgname,
    #          has_bbox=smal_has_bbox)

    # print("--> Exported param file: {0}".format(param_file))
    # print('*** FINISHED ***')

    return res_joints_preds

def load_model_from_disk(model_path, shape_family_id, load_from_disk, device):
    model = Model(device, shape_family_id, load_from_disk)
    model = nn.DataParallel(model)
    model = model.to(device)
    model.eval()

    if model_path is not None:
        # print( "found previous model %s" % model_path )
        # print( "   -> resuming" )
        model_state_dict = torch.load(model_path, map_location=device)

        own_state = model.state_dict()
        for name, param in model_state_dict.items():
            try:
                own_state[name].copy_(param)
            except:
                print("Unable to load: {0}".format(name))
    else:
        print('model_path is none')

    return model


def parse_opt():
    parser = argparse.ArgumentParser()
    parser.add_argument('--checkpoint', default='3501_00034_betas_v4.pth', help='Path to network checkpoint')
    parser.add_argument('--src_dir', default='', type=str, help='The directory of input images')
    # parser.add_argument('--result_dir', default='../demo_out', help='Where to export the output data')
    parser.add_argument('--shape_family_id', default=-1, type=int, help='Shape family to use')
    parser.add_argument('--batch_size', default=1, type=int)
    parser.add_argument('--gpu_ids', default='0', type=str, help='GPUs to use. Format as string, e.g. "0,1,2')
    opt = parser.parse_args()
    return opt

if __name__ == '__main__':
    opt = parse_opt()
    # os.environ['CUDA_VISIBLE_DEVICES'] = args.gpu_ids
    # print (os.environ['CUDA_VISIBLE_DEVICES'])
    # print("Let's use", torch.cuda.device_count(), "GPUs!")
    # assert torch.cuda.device_count() == 1, "Currently only 1 GPU is supported"
    # device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    print(get(**vars(opt)))
