import cv2
import numpy as np
from sklearn.cluster import KMeans


# def increase_brightness(img, value):
#   img_hsv = cv2.cvtColor(img, cv2.COLOR_RGB2HSV) # В HSV
#   h, s, v = cv2.split(img_hsv)
#   lim = 255 - value
#   v[v > lim] = 255
#   v[v <= lim] += value # Увеличение яркости
#   img_hsv = cv2.merge((h, s, v))
#   img = cv2.cvtColor(img_hsv, cv2.COLOR_HSV2BGR) # В RGB
#   return img
#
#
# def get_brightness(img):
#   img_hsv = cv2.cvtColor(img, cv2.COLOR_RGB2HSV) # В HSV
#   sum_brightness = np.sum(img_hsv[:,:,2]) # Сложить значения пикселей в V-канале
#   area = img.shape[0]*img.shape[1]
#   return round(sum_brightness/area, 2) # Средняя яркость


def crop_part(img, percent=0.25):
    x1, x2 = int(img.shape[0]*percent), int(img.shape[0]*(1-percent))
    y1, y2 = int(img.shape[1]*percent), int(img.shape[1]*(1-percent))
    return img[x1:x2, y1:y2]

def get_colors(img, n_clusters=3):
    img = crop_part(img)
    img = img.reshape((img.shape[0] * img.shape[1], 3))
    cluster = KMeans(n_clusters=n_clusters).fit(img) # Доминантные цвета
    centroids = cluster.cluster_centers_
    labels = np.arange(0, len(np.unique(cluster.labels_)) + 1) # Кол-во кластеров
    (hist, _) = np.histogram(cluster.labels_, bins = labels) # Гистограмма
    hist = hist.astype("float")
    hist /= hist.sum() # Нормализация
    colors = np.array(sorted([[percent, color] for percent, color in zip(hist, centroids)], reverse=True, key=lambda x: x[0]), dtype=object)

    start = 0
    palette = np.zeros((50, 300, 3), dtype=np.uint8)
    for percent, color in colors:
        end = start + (percent * 300)
        cv2.rectangle(palette, (int(start), 0), (int(end), 50), color.astype("uint8").tolist(), -1)
        start = end
    # palette = cv2.cvtColor(palette, cv2.COLOR_BGR2RGB)
    return palette, colors


def get_avgcol(palette):
    return list(map(round, palette.mean(axis=0).mean(axis=0)))


def get_tone(img):
    img = crop_part(img)
    hsv = cv2.cvtColor(img, cv2.COLOR_RGB2HSV)
    center = (0.4, 0.5)
    planka = 0.1
    all = cv2.inRange(hsv, (0, 255*0, 255*0), (180, 255*1.00, 255*1.00))
    lpart = cv2.inRange(hsv, (0, 255*center[0], 255*0), (180, 255*1.00, 255*1.00))
    dark1 = cv2.inRange(hsv, (0, 255*0.00, 255*0.00), (180, 255*1.00, 255*planka))
    dark2 = cv2.inRange(hsv, (0, 255*0.00, 255*planka), (180, 255*center[0], 255*center[1]))
    for s,v in zip(np.logspace(0.45, 1.00, 60, endpoint=True)/10, np.arange(center[1]+0.05, planka-0.01, -0.01)):
        part = cv2.inRange(hsv, (0, 255*center[0], 255*planka), (180, 255*s, 255*v))
        dark2 = cv2.bitwise_or(part, dark2)
    dark = cv2.bitwise_or(dark1, dark2)
    light = cv2.inRange(hsv, (0, 255*0.00, 255*center[1]), (180, 255*center[0], 255*1.00))
    multi = cv2.bitwise_xor(all, dark)
    multi = cv2.bitwise_xor(multi, light)
    multi = cv2.bitwise_and(multi, lpart)
    dark_part = cv2.countNonZero(dark)
    light_part = cv2.countNonZero(light)
    multi_part = cv2.countNonZero(multi)
    if multi_part > img.shape[0]*img.shape[1]*0.1:
        tone_class = 3 # class multi
    elif light_part > dark_part:
        tone_class = 2 # class light
    else:
        tone_class = 1 # class dark
    tone_parts = {"dark":dark_part, "light":light_part, "multi":multi_part}

    # testing
    # print(tone_parts, tone_class)
    # cv2.imshow("img", img)
    # target = "dark"
    # cv2.namedWindow(target, cv2.WINDOW_NORMAL)
    # cv2.resizeWindow(target, 200, 200)
    # cv2.imshow(target, cv2.bitwise_and(img, img, mask=dark))
    # target = "light"
    # cv2.namedWindow(target, cv2.WINDOW_NORMAL)
    # cv2.resizeWindow(target, 200, 200)
    # cv2.imshow(target, cv2.bitwise_and(img, img, mask=light))
    # target = "multi"
    # cv2.namedWindow(target, cv2.WINDOW_NORMAL)
    # cv2.resizeWindow(target, 200, 200)
    # cv2.imshow(target, cv2.bitwise_and(img, img, mask=multi))
    # cv2.waitKey(0)

    return tone_parts, tone_class


# def get_palette(img_obj_oc):
#     palette, obj_colors = get_colors(img_obj_oc)
#     obj_colors_prb = obj_colors[:,0]
#     obj_colors_rgb = obj_colors[:,1]
#     obj_colors_dict = [[round(obj_colors_prb[i], 4)]+list(map(round, obj_colors_rgb[i])) for i in range(len(obj_colors_prb))]
#     obj_colors_dict = list(map(lambda x: dict(zip(["part", "r", "g", "b"], x)), obj_colors_dict))
#     avg_color = get_avgcol(palette)
#     avg_color = dict(zip(["r", "g", "b"], map(round, avg_color)))
#     return obj_colors_dict, avg_color
